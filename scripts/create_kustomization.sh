#!/bin/bash

set -eo pipefail

echo "Creating kustomization.yaml at config/manager/kustomization.yaml"

cat > config/manager/kustomization.yaml << EOF
resources:
  - manager.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
images:
  - digest: will_be_replaced
    name: controller
    newName: registry.connect.redhat.com/gitlab/gitlab-runner-operator
EOF

echo "config/manager/kustomization.yaml:"
cat config/manager/kustomization.yaml